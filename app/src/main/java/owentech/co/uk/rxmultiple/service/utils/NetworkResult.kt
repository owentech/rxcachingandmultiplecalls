package owentech.co.uk.rxmultiple.service.utils

data class NetworkResult<out T>(val success: Boolean = false,
                                val resultObject: T? = null,
                                val error: Error? = null)

data class Error(val throwable: Throwable? = null,
                 val errorType: ErrorType? = null,
                 val errorCode: Int = 0)

enum class ErrorType {
    HTTP_ERROR,
    NETWORK_TRANSPORT_ERROR,
    PARSING_ERROR,
    MAIN_THREAD_EXCEPTION,
    UNKNOWN
}
