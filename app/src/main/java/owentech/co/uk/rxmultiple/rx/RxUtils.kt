package owentech.co.uk.rxmultiple.rx

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import owentech.co.uk.rxmultiple.service.utils.ApiException
import retrofit2.adapter.rxjava2.Result


class RxUtils {

    companion object {

        fun <T> networkTask(observable: Single<Result<T>>): Single<T> {
            return observable
                    .subscribeOn(Schedulers.io())
                    .flatMap { result ->
                        if (result.isError) {
                            Single.error<T>(result.error())
                        } else {
                            if (!result.response().isSuccessful) {
                                val bytes = result.response().errorBody()?.bytes()
                                bytes?.let {
                                    Single.error<T>(ApiException(result.response().code(), String(it)))
                                } ?: run {
                                    Single.error<T>(ApiException(result.response().code()))
                                }
                            } else {
                                Single.just(result?.response()?.body())
                            }
                        }
                    }
        }
    }

}