package owentech.co.uk.rxmultiple.rx

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.ReplaySubject
import owentech.co.uk.rxmultiple.data.DiskStorage
import owentech.co.uk.rxmultiple.service.utils.NetworkResult
import java.util.concurrent.TimeUnit

fun <T> Observable<NetworkResult<T>>.toReplaySubject(): ReplaySubject<NetworkResult<T>> {
    val subject = ReplaySubject.create<NetworkResult<T>>()

    this.subscribeOn(Schedulers.io())
            .subscribe(object : Observer<NetworkResult<T>> {
                override fun onComplete() {
                    subject.onComplete()
                }

                override fun onSubscribe(d: Disposable?) {

                }

                override fun onError(e: Throwable?) {
                    subject.onError(e)
                }

                override fun onNext(result: NetworkResult<T>) {
                    subject.onNext(result)
                    if (!result.success) {
                        subject.onComplete()
                    }
                }
            })

    return subject
}

fun <T> Observable<NetworkResult<T>>.persistResultToDisk(key: String, ttl: Long = 5L, timeUnit: TimeUnit = TimeUnit.MINUTES): Observable<NetworkResult<T>> {
    return this.doOnNext { result ->
        if (result.success) {
            DiskStorage.persistResultToDisk(key, result, ttl, timeUnit)
        }
    }
}

fun <T> ReplaySubject<NetworkResult<T>>?.shouldReturn(useCached: Boolean): Boolean {
    return (this != null && !this.hasComplete() ||
            this != null && this.hasComplete() && this.hasValue() && this.value.success && useCached)
}

fun <T> ReplaySubject<T>?.asObservable(): Observable<T> {
    return this as Observable<T>
}