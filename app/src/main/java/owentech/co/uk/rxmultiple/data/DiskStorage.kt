package owentech.co.uk.rxmultiple.data

import android.os.Looper
import io.paperdb.Paper
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions
import owentech.co.uk.rxmultiple.service.utils.NetworkResult
import java.util.concurrent.TimeUnit

class DiskStorage {

    companion object {

        const val CURRENT_WEATHER = "CurrentWeather"

        // todo look at Store library
        // todo syncronize
        fun <T> persistResultToDisk(key: String, result: NetworkResult<T>, ttl: Long = 5L, unit: TimeUnit = TimeUnit.MINUTES) {
            if (Looper.myLooper() == Looper.getMainLooper()){
                throw RuntimeException("Disk persistence on Main Thread not allowed")
            }

            val time = System.currentTimeMillis() + unit.toMillis(ttl)
            val pair: Pair<Long, NetworkResult<T>> = Pair(time, result)
            Paper.book().write(key, pair)
        }

        fun <T> getResultFromDisk(key: String): Observable<NetworkResult<T>> {
            return Observable.create { subscriber ->
                try {
                    val result: Pair<Long, NetworkResult<T>> = Paper.book().read(key)
                    subscriber.onNext(result.second)
                    subscriber.onComplete()
                } catch(e: Exception) {
                    subscriber.onError(e)
                }
            }
        }

        fun delete(key:String) {
            Paper.book().delete(key)
        }

        fun exists(key: String): Boolean {
            if (!Paper.book().exist(key)) {
                return false
            } else {
                val pair = Paper.book().read<Pair<Long, Any>>(key)
                if (System.currentTimeMillis() < pair.first) {
                    return true
                } else {
                    Paper.book().delete(key)
                    return false
                }
            }
        }
    }

}