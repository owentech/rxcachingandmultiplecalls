package owentech.co.uk.rxmultiple.main

import owentech.co.uk.rxmultiple.data.model.CurrentWeatherResponse
import owentech.co.uk.rxmultiple.service.utils.Error

interface MainView {

    fun weatherUpdated(currentWeatherResponse: CurrentWeatherResponse?)
    fun error(errorMessage: String)
}