package owentech.co.uk.rxmultiple.data.model

data class CurrentWeatherResponse(val name: String)