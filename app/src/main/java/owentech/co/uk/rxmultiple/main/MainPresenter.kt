package owentech.co.uk.rxmultiple.main

import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import owentech.co.uk.rxmultiple.MyApp
import owentech.co.uk.rxmultiple.data.model.CurrentWeatherResponse
import owentech.co.uk.rxmultiple.service.utils.ApiException

class MainPresenter(val view: MainView) {

    var ongoingRequest: Disposable? = null

    fun getWeather(useCached: Boolean = false) {

        if (ongoingRequest?.isDisposed ?: true) {

            MyApp.apiRepository.newGetCurrentWeather(useCached)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(object: SingleObserver<CurrentWeatherResponse>{
                        override fun onSubscribe(d: Disposable?) {
                            ongoingRequest = d
                        }

                        override fun onSuccess(currentWeatherResponse: CurrentWeatherResponse?) {
                            view.weatherUpdated(currentWeatherResponse)
                            ongoingRequest?.dispose()
                        }

                        override fun onError(e: Throwable?) {
                            if (e is ApiException) {
                                view.error("Api Error: " + e.errorCode)
                            } else {
                                view.error("Error: " + e?.message)
                            }
                            ongoingRequest?.dispose()
                        }
                    })
        }
    }

}