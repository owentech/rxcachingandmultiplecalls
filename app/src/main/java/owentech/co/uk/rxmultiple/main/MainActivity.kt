package owentech.co.uk.rxmultiple.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import owentech.co.uk.rxmultiple.R
import owentech.co.uk.rxmultiple.data.model.CurrentWeatherResponse

class MainActivity : AppCompatActivity(), MainView {

    val presenter = MainPresenter(this)

    lateinit internal var get: Button
    lateinit internal var getCached: Button
    lateinit internal var resultText: TextView
    lateinit internal var progress: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        get = findViewById(R.id.get) as Button
        getCached = findViewById(R.id.getCached) as Button
        resultText = findViewById(R.id.resultText) as TextView
        progress = findViewById(R.id.progress) as ProgressBar

        get.setOnClickListener {
            resultText.text = ""
            progress.visibility = View.VISIBLE
            presenter.getWeather(false)

        }

        getCached.setOnClickListener {
            resultText.text = ""
            progress.visibility = View.VISIBLE
            presenter.getWeather(true)
        }
    }


    override fun weatherUpdated(currentWeatherResponse: CurrentWeatherResponse?) {
        progress.visibility = View.GONE
        resultText.text = currentWeatherResponse?.name
    }

    override fun error(errorMessage: String) {
        progress.visibility = View.GONE
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }
}
