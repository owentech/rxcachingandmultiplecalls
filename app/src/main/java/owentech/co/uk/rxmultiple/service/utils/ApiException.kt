package owentech.co.uk.rxmultiple.service.utils

class ApiException(val errorCode: Int, val errorBody: String = ""): Exception() {

}