package owentech.co.uk.rxmultiple.repo

import com.nytimes.android.external.store3.base.impl.MemoryPolicy
import com.nytimes.android.external.store3.base.impl.Store
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import io.reactivex.Single
import owentech.co.uk.rxmultiple.data.model.CurrentWeatherResponse
import owentech.co.uk.rxmultiple.rx.*
import owentech.co.uk.rxmultiple.service.ApiService
import retrofit2.adapter.rxjava2.Result
import java.util.concurrent.TimeUnit

class ApiRepository {

    val CURRENT_WEATHER = 1

    val service = ApiService()

    val currentWeatherStore: Store<CurrentWeatherResponse, Int> by lazy {
        generateSimpleStore<CurrentWeatherResponse, Int>(serviceCall = {service.getCurrentWeather()})
    }


    fun newGetCurrentWeather(cached: Boolean): Single<CurrentWeatherResponse> {
        if (cached) {
            return currentWeatherStore.get(CURRENT_WEATHER)
        } else {
            return currentWeatherStore.fetch(CURRENT_WEATHER)
        }
    }

    fun <T, K> generateSimpleStore(expireAfter: Long = 10, timeUnit: TimeUnit = TimeUnit.SECONDS, serviceCall: () -> Single<Result<T>>): Store<T, K> {

        return StoreBuilder.key<K, T>()
                .fetcher {
                    RxUtils.networkTask(serviceCall())
                }
                .memoryPolicy(
                        MemoryPolicy
                                .builder()
                                .setExpireAfterWrite(expireAfter)
                                .setExpireAfterTimeUnit(timeUnit)
                                .build()
                )
                .open()
    }

//    fun <T, K> generateStore(f: () -> Result<T>): Store<T, K> {
//        return StoreBuilder.parsedWithKey<K, Result<T>, NetworkResult<T>>()
//                .fetcher {
//                    key ->
//                    f()
//                    service.getCurrentWeather(key)
//                }
//                .parser { _, retrofitResult ->
//                    RxUtils.mapToNetworkResult(retrofitResult)
//                }
//                .memoryPolicy(
//                        MemoryPolicy
//                                .builder()
//                                .setExpireAfterWrite(10)
//                                .setExpireAfterTimeUnit(TimeUnit.SECONDS)
//                                .build()
//                )
//                .open()
//    }

}