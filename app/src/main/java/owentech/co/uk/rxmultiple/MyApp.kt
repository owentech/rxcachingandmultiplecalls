package owentech.co.uk.rxmultiple

import android.app.Application
import io.paperdb.Paper
import owentech.co.uk.rxmultiple.repo.ApiRepository

class MyApp: Application() {

    companion object {
        val apiRepository: ApiRepository by lazy { ApiRepository() }
    }

    override fun onCreate() {
        super.onCreate()
        Paper.init(this)
    }
}