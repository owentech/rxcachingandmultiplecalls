package owentech.co.uk.rxmultiple.service

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import owentech.co.uk.rxmultiple.data.model.CurrentWeatherResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.Result
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

class ApiService {

    val weatherService: WeatherService
    val okHttpClient: OkHttpClient by lazy {
        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        okHttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        okHttpClientBuilder.addInterceptor({
            chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("APPID", "e324c0a073147849397b96434a973aa6")
                    .addQueryParameter("units", "imperial")
                    .build()

            val requestBuilder = original.newBuilder()
                    .url(url)

            val request = requestBuilder.build()
            chain.proceed(request)

        })

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClientBuilder.addInterceptor(loggingInterceptor)

        okHttpClientBuilder.build()
    }


    interface WeatherService {
        @GET("weather")
        fun currentWeatherById(@Query("id") id: Long?): Single<Result<CurrentWeatherResponse>>
    }

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        weatherService = retrofit.create(WeatherService::class.java)
    }

    fun getCurrentWeather(id: Long = 2172797): Single<Result<CurrentWeatherResponse>> {
        return weatherService.currentWeatherById(id)
    }

}